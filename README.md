hooch-hydro.py
===============

"hooch-hydro.py" is a command line program that can generate several hydrology
plots from USGS rdb files.


Documentation
-------------
This README file (that you are now reading) contains a bunch of useful hints
about the general operation of hooch-hydro.py.

All the README files (.html, .md, .txt, .pfd, etc) contain the same information, 
but in different formats for your convenience.

There is detailed help for the program with the `--help` flag, both for the main
prgram and for the individual modules. The `--help` text for all modules is
included at the end of this file.

Running `hooch-hydro.py`
-------------------------

The command to run the program will generally take this form:

    In [1] %run hooch-hydro.py [options] [module] [module specific options]


Workflow
---------------------

I envision you using the program like this:

  1. Start the investigation by choosing a station or file.
    - If you have a station ID, just use that.
    - If you have an `.rdb` file on you computer, you might move it into the 
    same directory as the hooch-hydro.py script, or a subdirectory similar to 
    `sample-data/`. This will simply make using the `--file path/to/file.rdb`
    flag much shorter command to type in. 

  2. Run thru a few of the modules to see what the data looks like. The 
  `cont-timesers` plot should show "gaps" in the data set where there are
  missing values (i.e. Quality column is 'P' or 'P:e'). It is also a good idea
  to glance at the terminal output when running the program to make sure that
  everything makes sense. For example from this section, you can see that there
  is no was no data that was dropped due to having P or P:e in the 
  quality column:

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    In[1]: %run hooch-hydro.py --stationid 15300520 cont-timesers

    ... omitted for clarity ...

      1856 rows of data...
      1856 non-null discharge readings
      1856 non-null quality readings
      dropping all 'P' and 'P:e' data...
        ignoring discharge with 'P' in quality column. 1856 remaining
        ignoring discharge with 'P:e' in quality column. 1856 remaining
      drop quality column...
      convert index to actual pandas.DatetimeIndex
      expand data frame to include (empty) places for every point in period of record
      Period of Record: 2008-06-01 00:00:00 to 2013-06-30 00:00:00

      ... omitted for clarity ...
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Where as in this exmaple you can see that the period of record is not
    continuous:

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    In[1]: %run hooch-hydro.py --stationid 15300520 cont-timesers

    ... omitted for clarity ...

      Period of Record: 1963-06-23 00:00:00 to 2013-09-30 00:00:00
      18363 days in period of record
      1927 days with data in period of record
      10.49% of period of record has data

      ... omitted for clarity ...
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  3. When you have found a dataset and combination of plot options that you
  are happy with, then use the up-arrow to recall the command, and arrow back
  to add the `--save` flag to the command.
  
  4. Move the generated file to your final location and/or re-name it so that
  subsequent runs of the program don't overwrite your file.
  
  5. If you keep a copy of the successful commands that you run in a text file
  you can _exactly_ duplicate your work in the future.


Errors / Problems
-----------------

Try to send the the exact command that gave you the problem and any text that is
output to the console. You can just copy and paste into an email.

Examples
------------------

Make a staticitical plot for the period of record, displaying min, max and mean.
Save the output.

    In[1]: %run hooch-hydro.py --save --stationid 15300520 stats --traces min max mean


Make probability exceedance curve for julian days 100 to 110.

    In[1]: %run hooch-hydro.py --stationid 15300520 durcur -j 100 110


### General Command Line Info ###

It is assumed that you will be running this program through an IPython terminal.

Generally command line help is shown like this:

    In [1]: some command you type
    Output from the command...
    
    
The `In [1]: ` portion is a generic symbol for your terminal prompt. If using 
IPython, then each time you type a command, the number will increment. If you
are not using IPython, you may have an entirely differnet prompt. All you have 
to remember is that you type the text after the prompt, but not the prompt
itself.

If you are reading documentation for other program, the generic prompt is often
shown as simply a '$', like this:

    $ some command you type
    Output from the command...
    
### Command Line Hints ###

* Recall last command with the up arrow key.

* Use tab to auto complete as you type. This is helpful for long file names.
It won't work for options to the program. It basically just works for files and 
paths on your computer.

* Paths to files or folders with spaces in the name are tricky. Usually you have
to "escape" the space in the name by adding a slash charachter before the space 
or by using quotes around the path. E.g.:

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    C:\Somewhere\folder/ with/ spaces/ in/ the/ name\file.rdb
    "C:\Somewhere\folder with spaces in the name\file.rdb"
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


### Common Commands ###

Run the program
   
    In[1]: %run hooch-hydro.py

Change directories
   
    In[1]: cd C:\path\to\some\place\on\your\computer

Show current directory ("Print working directory")

    In[1]: pwd

List files in a directory
   
    In[1]: ls
    
    
More info:

[IPython Home Page](http://ipython.org/index.html)

[Canopy Help](http://docs.enthought.com/canopy/quick-start/code_editor.html)




Cathy Specific Help
=========================

Storing and updating
---------------------

I will deliver new versions of the program as `hooch-vX.X.X.zip` files. You will
unzip the folder and move all of the contents to `C:\Hooch_programs\`. So for 
example, your `C:\Hooch_programs\` directory might look like this:

    C:\Hooch_programs\
       |
       |- hooch-v0.0.3\
       |     |- hooch-hydro.py
       |     |- docs\
       |     |- README.pdf
       |     
       |- hooch-v0.0.4\
       |    |- hooch-hydro.py
       |    |- docs\
       |    |- README.pdf
    
each of which will have a seperate version of hooch in it. If I send you a new
version, you'll need to make sure that the shortcut you created on your desktop 
is linking to the correct version!!

Running
-------------------

We found two methods of running the program on Cathy's Windows 8 computer.
1. Using Canopy
2. Using the Pylab shell

I suspect that using Canopy will be easier in the long run as the terminal will
let you copy/paste text in and out of it. Also you should be able to change the 
width of the terminal window with Canopy and I doubt that will work with the
Pylab Shell.

To run with Canopy:

  1. Double click the Canopy desktop shortcut
  2. Change the working directory to match the editor directory
  3. Find the console (terminal) panel in the Canopy window.
  3. Run the program using the `%run hooch-hydro.py` followed by whatever
  options/modules you need to make it do what you want.
  
To run with the PyLab shell:

  1. Double-click the PyLab shortcut on the Desktop
  2. use the `cd` command to move yourself to the directory with 
  the correct version of `hooch-hydro.py`
  3. Run the program using the `%run hooch-hydro.py` command followed by 
  whatever options/modules you need to make it do what you want.
  

