#!/usr/bin/env python
#
# hooch-hydro.py
#
# A multi-module plotting script for a couple hydrology related tasks.
# see help maintained in the command line --help flag or the README
#
# Tobey Carman, by request for Cathy Flanagan
# Started Oct 2013
#
#
# describes rdb format
# https://phpbbservices.com/WebServices/uv_class.html#WhatRDB
#
#
# http://help.waterdata.usgs.gov/faq/automated-retrievals
#

import sys
try:
  import os
  import argparse
  import textwrap
  import numpy as np
  import matplotlib.pyplot as plt
  import matplotlib.dates as mdates
  import matplotlib.cm as cmap
  import datetime as dt
  import pandas as pd
  import tempfile
  import contextlib
  from scipy.interpolate import griddata

except ImportError as e:
  print "%s" % e
  sys.exit()

from IPython import embed


def main():

  #
  # Top level hooch parser
  #
  parser = argparse.ArgumentParser(
    formatter_class=argparse.RawTextHelpFormatter,
    description=textwrap.dedent('''\
      Hooch - a homemade alternative to Wiskee.
      
      Hooch will generate a variety of hydrograph plots, 
      duration curves, and a probablity exceedance table
      from USGS .rdb files. Hooch can read local files
      or fetch rdb files from the USGS web servers.
      '''
    ),
    epilog=textwrap.dedent('''\
      This program is in beta, so expect ugly failures
      with rather ugly error messages. Send questions to
      Tobey Carman <tobeycarman@gmail.com>

        Some example stations:
        -----------------------
        15300520 (Kaskanak Creek)
        15302200 (S Fork Koktuli River)
        15302250 (N Fork Koktuli River)
        15300250 (Upper Talarik Creek)
        15301500 (Allen R NR Aleknagik)
      '''
    )
  )


  # OPTIONS THAT APPLY TO ALL SUBPARSERS
  parser.add_argument('--save', action='store_true', 
    help=textwrap.dedent('''\
      The plot shown will also be saved in 
      the current working directory with the 
      name:
          'hooch-MODULENAME.pdf'
          
      '''
    )
  )
  
  ## Haven't implemented this yet...(may not need it?)
  #parser.add_argument('--display', action='store_true',
  #  help=textwrap.dedent('''\
  #    Display Plots. Requires a backend...
  #  ''')
  #)

  # MUTUALLY EXCLUSIVE PARSER GROUP.
  #   -- you must provide a file or station id, but not both.
  mex_par_grp_datasrc = parser.add_mutually_exclusive_group(required=True)

  mex_par_grp_datasrc.add_argument('--file',
    help=textwrap.dedent('''\
      Path to a (local) rdb file to use
      
      '''
    )
  )
  mex_par_grp_datasrc.add_argument('--stationid',
    help=textwrap.dedent('''\
      A USGS station id. Hooch will try to 
      download the period of record from:
      http://waterservices.usgs.gov/nwis/dv/
      '''
    )
  )

  #
  # Define MODULES (sub-programs) of hooch
  #
  subparsers = parser.add_subparsers(
    title='MODULES',
    description=textwrap.dedent('''\
      Choose ONE of the following modules. Each module has a --help flag
      that provides more options for that module.
      '''
    ),
    help=textwrap.dedent('''\
      
      '''
    )
  )

  # CONTINUOUS TIME-SERIES
  parser_contts = subparsers.add_parser('cont-timesers',
    help=textwrap.dedent('''\
      Show a continuous timeseries for 
      the period of record.
      
      '''
    )
  )
  parser_contts.set_defaults(func=continuous_timeseries)

  # OVERLAY GRAPHS
  parser_overlay = subparsers.add_parser('overlay',
    help=textwrap.dedent('''\
      Overlay all years in a period-of-record 
      on a generic year.
      
      '''
    )
  )
  parser_overlay.add_argument('--wateryear', action='store_true', 
    help=textwrap.dedent('''\
      Display using water year instead of normal calendar year
    ''')
  )
  parser_overlay.add_argument('--calmonth', action='store_true', default=False,
    help=textwrap.dedent('''Show dates as "Jan, Feb, ..." ''')
  )
  parser_overlay.add_argument('--traces', nargs='*', default=None,
    choices=['std','min','max','mean','median'],
    help=textwrap.dedent('''A list of stats to plot.''')
  )
  parser_overlay.set_defaults(func=overlay)

  # SUMMARY STATS
  parser_stats = subparsers.add_parser('stats',
    help=textwrap.dedent('''\
      Plot a variety of statistics about a 
      period of record.
      
      '''
    )
  )
  parser_stats.add_argument('--wateryear', action='store_true', 
    help=textwrap.dedent('''\
      Display using water year instead of normal calendar year
      '''
    )
  )
  parser_stats.add_argument('--calmonth', action='store_true', default=False,
    help=textwrap.dedent('''Show dates as "Jan, Feb, ..."''')
  )
  parser_stats.add_argument('--traces', nargs='*', required=True, default=None,
    choices=['std','min','max','mean','median'],
    help=textwrap.dedent('''A list of stats to plot.''')
  )
  parser_stats.set_defaults(func=stats_only)
  
  # DURATION CURVE
  parser_durcur = subparsers.add_parser('durcur',
    help=textwrap.dedent('''Make duration curve.''')
  )
  parser_durcur.add_argument('-p','--preview', action='store_true', 
    help=textwrap.dedent('''\
      Show a continuous timeseries first with only the selected data.
      '''
    )
  )
  
  group = parser_durcur.add_mutually_exclusive_group()
  group.add_argument('-j','--julian', nargs=2,
    help=textwrap.dedent('''range to cover in julian days''')
  )
  group.add_argument('-m','--month', nargs=1, type=int,
    choices=(1,2,3,4,5,6,7,8,9,10,11,12),
    help=textwrap.dedent('''A month to plot''')
  )
  group.add_argument('--mdmd', nargs=2,
    help=textwrap.dedent('''\
      specify as two strings "m,d" "m,d" for start and end of range
      '''
    )
  )

  parser_durcur.set_defaults(func=duration_curve)

  # TO EXCEL
  # parser_toexcel = subparsers.add_parser(
  #     'toexcel', help="Dump a variety of data to various excel or comma delimited formats.")
  # parser_toexcel.add_argument('--delimiter', choices=[',', 'tab', 'space'], 
  #     nargs='*', default=None, help='The delimiter to use')
  # parser_toexcel.add_argument('--pextable', action='store_true', 
  #                             help='Write out the ones-by-tens probability exceedance matrix')
  # parser_toexcel.set_defaults(func=toexcel)

  
  args = parser.parse_args()   
  
  #print args
  #fig = plt.figure()
  #cid = fig.canvas.mpl_connect('button_press_event', onclick)
  args.func(args)
  
def stats_only(args):
  df, md = load_data(args)
  
  print "preparing the stats only hydrograph..."
  ord_days = np.arange(1, 367)
  
  # deal with the statistics...
  stats_axes = build_stats_axes(args, df)    

  # plot the tick marks...
  handle_timeax_locs_and_labels(args)

  plt.xlim(ord_days[0], ord_days[-1]) # fix axis show ONLY a year
  plt.title(md['station-desc'])
  plt.ylabel('Discharge (cfs)')
  plt.xlabel('Day of Year')
  plt.grid(which='both')
  plt.legend(loc='best')
  # remove proxy artists now that we have called plt.legend(...)
  if 'mm_fill_pa' in stats_axes:
    stats_axes['mm_fill_pa'].pop(0).remove()
  if 'std_fill_pa' in stats_axes:
    stats_axes['std_fill_pa'].pop(0).remove()
  if args.save:
    print "Saving file..."
    plt.savefig('hooch-stats.pdf')

  plt.show()
  
  
# shows all years overlain on top of eachother. optionally stats too.
def overlay(args):
  df, md = load_data(args)
  
  print "preparing the overlay hydrograph..."
  ord_days = np.arange(1, 367)

  from itertools import cycle
  lines = ['-', '--', '-.', ':', ]
  linecycler = cycle(lines)

  print "  plotting data for each year..."
  for i, yr in enumerate(set(df.index.year)):
    yrstr = '%s'%yr
    #print yrstr, "--> water year 10-1-%s to 9-30-%s" % (yr-1, yr)     
    #wyidx = pd.date_range(start='10/1/%s'%(yr-1), end='9/30/%s'%(yr), freq='D')
    #print wyidx
  
    # convert the timestamp index to julian or ordinal day
    jidx = [pdts2ord(ts) for ts in df[yrstr].index]

    # get the actual data values; may be less than 365
    discharge_vals = [v for v in df[yrstr].discharge]

    # make a new series, indexed by julian day
    s = pd.Series(discharge_vals, jidx)

    # reindex based on a full year (filling with NaN for missing values)
    s = s.reindex(ord_days)

    c = 'k'
    if args.wateryear:
      print "    using wateryear; rolling %s forward 91 days..." % yr
      s = pd.Series(np.roll(s.values, 91))

    # Check that the year has data and should be included in the legend
    if np.isnan(np.max(s)):
      legend_label = None
    else:
      legend_label = yrstr

    # plot the data
    plt.plot(ord_days, s,
             linewidth=.75,
             linestyle=next(linecycler),
             color=c,
             label=legend_label)

  
  # get all the statistics axes that the user specified (plotted)
  stats_axes = build_stats_axes(args, df)    
  
  # plot the tick marks...
  handle_timeax_locs_and_labels(args)

  plt.xlim(ord_days[0], ord_days[-1]) # fix axis show ONLY a year
  plt.title(md['station-desc'])
  plt.ylabel('Discharge (cfs)')
  plt.xlabel('Day of Year')
  plt.grid(which='both')
  plt.legend(loc='best')

  # remove proxy artists now that we have called plt.legend(...)
  if 'mm_fill_pa' in stats_axes:
    stats_axes['mm_fill_pa'].pop(0).remove()
  if 'std_fill_pa' in stats_axes:
    stats_axes['std_fill_pa'].pop(0).remove()

  if args.save:
    print "Saving file..."
    plt.savefig('hooch-overlay.pdf')

  
  plt.show()

# Show all years end to end... a continuous time series for the period of record.
def continuous_timeseries(args):
  df, md = load_data(args)
  

  '''
  # http://stackoverflow.com/questions/14125172/how-to-index-a-dataframe-with-holes-with-a-timeseries-without-holes
  
  In [3]: pd.DatetimeIndex(start="1934-04-01 00:00:00", end="2014-01-22 00:00:00", freq='D')
  Out[3]:
  <class 'pandas.tseries.index.DatetimeIndex'>
  [1934-04-01 00:00:00, ..., 2014-01-22 00:00:00]
  Length: 29152, Freq: D, Timezone: None

  In [4]: df
  Out[4]:
  <class 'pandas.core.frame.DataFrame'>
  DatetimeIndex: 13446 entries, 1934-04-01 00:00:00 to 2014-01-22 00:00:00
  Data columns (total 1 columns):
  discharge    13333  non-null values
  dtypes: float64(1)

  In [5]: df.reindex(pd.DatetimeIndex(start="1934-04-01 00:00:00", end="2014-01-22 00:00:00", freq='D'))
  Out[5]:
  <class 'pandas.core.frame.DataFrame'>
  DatetimeIndex: 29152 entries, 1934-04-01 00:00:00 to 2014-01-22 00:00:00
  Freq: D
  Data columns (total 1 columns):
  discharge    13333  non-null values
  dtypes: float64(1)
  '''
  
  print "preparing the traditional hydrograph long-timeseries plot..."
  p = df.plot()
  p.set_title(md['station-desc'])
  p.set_ylabel('Discharge (cfs)')
  p.set_xlabel('Date')
  
  if args.save:
    print "Saving file..."
    plt.savefig('hooch-cont-timesers.pdf')

  plt.show()

# make duration curves...
def duration_curve(args):
  df, md = load_data(args)
  print "creating the probability exceedance curve..."
  #print df

  # default to the entire year 
  sjday = 1
  ejday = 366

  # refine date range based on user selection
  if args.julian:
    if not validate_julian_range(args.julian):
      print "WARNING: strange values for julian date range: %s" % args.julian
    sjday = int(args.julian[0])
    ejday = int(args.julian[1])
  elif args.month:
    # no need to validate - constrained by arparse choices{}
    arbitrary_yr = 2012 #  Choose because it has a leap year
    thism = args.month[0]
    if thism < 12:
      nextm = thism + 1
      s = dt.date(year=arbitrary_yr, month=thism, day=1)
      e = dt.date(year=arbitrary_yr, month=nextm, day=1) - dt.timedelta(days=1)
    else:
      nextm = 1
      next_arbitrary_yr = arbitrary_yr + 1
      s = dt.date(year=arbitrary_yr, month=thism, day=1)
      e = dt.date(year=next_arbitrary_yr, month=nextm, day=1) - dt.timedelta(days=1)

    sjday = s.timetuple().tm_yday
    ejday = e.timetuple().tm_yday
  elif args.mdmd:
    s, e = args.mdmd
    sm, sd = s.split(',')
    em, ed = e.split(',')
    sjday = md2ord_leap(int(sm), int(sd))
    ejday = md2ord_leap(int(em), int(ed))
  
  od_idx = np.array([pdts2ord(ts) for ts in df.index])
  

  print "  selecting only data between Julian Day %s and %s" % (sjday, ejday)

  # user specified a range that should wrap
  if sjday > ejday:
    print "  NOTE: Range wraps end of calendar year!!"
    # NOTE: this seems to require the bitwise operators (& and |)
    # Not sure I really understand it...
    user_ma_eoy = ( ((od_idx >= sjday) & (od_idx <= 366)) )
    user_ma_boy = ( ((od_idx <= ejday) & (od_idx >= 0)) )
    user_ma = (user_ma_eoy | user_ma_boy)
  else:
    user_ma = ((od_idx >= sjday) & (od_idx <= ejday))

  # user ma seems to be a boolean array (mask)...    
  reduced_data = df[user_ma]

  if not (len(reduced_data) > 0):
    print "ERROR: Selection must be too tight?"
    print "Quitting..."
    sys.exit(-1)
    
  print "  some general statistics for the selected date range over the period of record:"
  theNumbers = reduced_data.dropna()['discharge'].values
  period_min      = np.min(theNumbers)
  period_max      = np.max(theNumbers)
  period_mean     = np.mean(theNumbers)
  period_median   = np.median(theNumbers)
  period_std      = np.std(theNumbers)
  period_variance = np.var(theNumbers)
  period_hist     = np.histogram(theNumbers) # 2 arrays: histogram and bin edges
  
  print "   {1:>10.3f} {0}".format("min", period_min)
  print "   {1:>10.3f} {0}".format("max", period_max)
  print "   {1:>10.3f} {0}".format("mean", period_mean)
  print "   {1:>10.3f} {0}".format("median", period_median)
  print "   {1:>10.3f} {0}".format("std", period_std)
  print "   {1:>10.3f} {0}".format("variance", period_variance)
  print "   hist:", [v for v in period_hist[0]]
  #reduced_data.plot(marker='o', style='', label='pandas plotter')
  if args.preview:
    print "  displaying a preview of the selected data on a continuous time series..."
    plt.plot(reduced_data.index, reduced_data, marker='^', linestyle='', label='selected data')
    plt.title( "%sChoosing Julian Day %s to %s" % (md['station-desc'], sjday, ejday) )
    plt.xlabel("Date")
    plt.ylabel("Discharge (cfs)")
    plt.legend(loc='best')
    if args.save:
      print "Saving file..."
      plt.savefig('hooch-pex-preview.pdf')
    plt.show()
  
  print "  calculating the probability of exceedance for selected range..."
  # discharge, sorted greatest to least, removing duplicate values.
  q = sorted(set(reduced_data.dropna()['discharge'].values), reverse=True)
  pex = [100.0*(i+1.0)/(len(q)+1) for i, v in enumerate(q)]

  print "  yields %s distinct discharge and %s corresponding prob. ex. values..." % (len(q), len(pex))
  print "  re-griding data to even spacing..."
  gridx = np.arange(0, 101, 1)
  grid_linear = griddata(np.array(pex), np.array(q), gridx, method='linear')
  #grid_nearest = griddata(np.array(pex), np.array(q), gridx, method='nearest')
  #grid_cubic = griddata(np.array(pex), np.array(q), gridx, method='cubic')

  # build some abbrvs for the title
  stnd = md['station-desc'][:-1] # strip newline
  syr = reduced_data.index[0].year
  eyr = reduced_data.index[-1].year
  smd = ord2md_leap(sjday)
  emd = ord2md_leap(ejday)
  
  # build the title
  title = '''Probability of Exceedance Curve
%s
Analysis Years: %s - %s
%s to %s (Julian Day %s to %s)  
''' % (stnd, syr, eyr, smd, emd, sjday, ejday)

  print ""
  print title
  print_pex_table(gridx, grid_linear)
  print ""
  print "{0:<16s} {1:>10.3f}".format("Period Mean", period_mean)
  print "{0:<16s} {1:>10.3f}".format("Period Median", period_median)
  print ""


  pex_ax = plt.plot(pex, q, label='duration curve', linestyle='-')
  #pex_ax_l = plt.plot(gridx, grid_linear, label='linear interp', marker='o', linestyle='--')
  #pex_ax_n = plt.plot(gridx, grid_nearest, label='nearest interp')
  #pex_ax_c = plt.plot(gridx, grid_cubic, label='cubic interp', marker='2', linestyle=':')
  plt.grid(which='both')
  plt.xlim(0, 100)
  plt.title(title)
  plt.xlabel("Percent Chance of Exceedance")
  plt.ylabel("Discharge (cfs)")
  plt.legend()
  plt.tight_layout()

  if args.save:
    print "  saving probability exceedance plot..."
    plt.savefig('hooch-durcur.pdf')
    
    print "  saving pex table..."
    with open("hooch-durcur-table.txt", "w") as text_file:
      print "  opening file, and temporarily redirecting stdout there..."
      with stdout_redirect(text_file):
        print title
        print_pex_table(gridx, grid_linear)
        print ""
        print "{0:<16s} {1:>10.3f}".format("Period Mean", period_mean)
        print "{0:<16s} {1:>10.3f}".format("Period Median", period_median)
        print ""

      print "  done saving file..."
  print "  showing prob. ex plot..."
  plt.show()




# not implemented yet....
def toexcel(args):
  pass


# 
# Utilities...
# 

def build_stats_axes(args, df):
  '''Return a list of axes instances (already plotted), one for each stat trace.
  
  Included are proxy artists for each fill-between. After calling legend, 
  remove these using .pop().remove()
  '''
  rd = {}  #  return list - a dict with all axes and proxy artists that have 
           #  been added and should be sent back to the caller...

  print "  buidling statistics trace(s)..."
  grouped = df.groupby(lambda x: x.timetuple()[7]) # group by day of year
  ord_days = np.arange(1, 367)
  print "  checking length of arrays for plotting..."
  if len(ord_days) != len(grouped):
    print "  ERROR! Somehow the arrays are not the right length..."

  csd = {}  # collected stats data
#   if len(args.traces) == 0:
#     print "Error: You must specify at least one trace!" #  <- is this handled with argparse?
#     sys.exit(-1)

  if args.traces:
    for trace in args.traces:
      stat_function = getattr(grouped, trace, None)
      if stat_function is None:
        print "  ERROR: Something is messed up? cant find stat function..."
      else:
        csd['%s_df'%trace] = stat_function()
        csd['%s_df'%trace].columns = [trace]
    #print csd
  else:
    print "  no stats to process; returning empty dict..."
    return {}

  if args.wateryear:
    print "  using wateryear, rolling stats data forward 91 days..."
    for key in csd:
      csd[key] = pd.DataFrame(np.roll(csd[key], 91))

  print "  plotting traces..."
  # min and max are gray dots...
  if 'min_df' in csd:
    rd['min_axes'] = plt.plot(ord_days, csd['min_df'], linestyle=':', color='0.8', linewidth=1.0, label='min')
  if 'max_df' in csd:
    rd['max_axes'] = plt.plot(ord_days, csd['max_df'], linestyle=':', color='0.8', linewidth=1.0, label='max')

  # mean, and median are default colors
  if 'mean_df' in csd:
    rd['mean_axes'] = plt.plot(ord_days, csd['mean_df'], linewidth=3.0, linestyle='-', label='mean')

  if 'median_df' in csd:
    rd['median_axes'] = plt.plot(ord_days, csd['median_df'], linewidth=3.0, linestyle='--', label='median')

  # if we have both min and max, then fill between and remove the traces...
  if ('min_df' in csd) and ('max_df' in csd):
    rd['mm_fill'] = plt.fill_between(
        ord_days, 
        np.reshape(csd['min_df'].values, len(csd['min_df'])), 
        np.reshape(csd['max_df'].values, len(csd['max_df'])), 
        color='gray', alpha='0.15')

    # plot a proxy artist for the legend, 
    # then get rid of the min/max traces...                                     
    # (for some reason alpha='.15' is not working to spec color.)
    rd['mm_fill_pa'] = plt.plot([1,],[1,], color=(0,0,0,0.15), linewidth=3.0, label='min/max')
    rd['min_axes'].pop(0).remove()          
    rd['max_axes'].pop(0).remove()

  # handle the std trace. Should always get plotted with a mean trace...
  if 'std_df' in csd:
    if 'mean_df' not in csd:            
      csd['mean_df'] = grouped.mean()   
      if args.wateryear:  # case where user did not specify mean, but did spec. wateryear
        csd['mean_df'] = pd.DataFrame(np.roll(csd['mean_df'], 91))

      rd['mean_axes'] = plt.plot(ord_days, csd['mean_df'], linewidth=3.0, linestyle='-', label='mean')

    prstd = np.reshape(csd['std_df'].values, len(csd['std_df']))
    prmean = np.reshape(csd['mean_df'].values, len(csd['mean_df']))
    
    rd['lessstd_axes'] = plt.plot(ord_days, prmean + prstd, linestyle=':', color='red', linewidth=1.0,)# label='+1 std' )
    rd['plusstd_axes'] = plt.plot(ord_days, prmean - prstd, linestyle=':', color='red', linewidth=1.0,)# label='-1 std' )

    rd['std_fill'] = plt.fill_between(
        ord_days, 
        prmean + prstd, 
        prmean - prstd, 
        color='red', alpha='.15',)# label='+/- 1 std')
   
    # proxy artist for the legend and remove the traces....
    rd['std_fill_pa'] = plt.plot([1,],[1,], color=(1,0,0,0.15), linewidth=3.0, label='+/- 1 std')
    rd['lessstd_axes'].pop(0).remove() # <- not working! still show up in legend!
    rd['plusstd_axes'].pop(0).remove()
  
  return rd


# loading data...
def dl_from_waterservices(stationid):
  import urllib2
  url = "http://waterservices.usgs.gov/nwis/dv/?site=%s&format=rdb&parameterCd=00060&startDT=1900-01-01" % stationid
  print "trying to download this: %s" % url
  response = urllib2.urlopen(url)
  if response.code != 200:
    print "Something is fucked..."
  rawdata = response.read()
  return rawdata.splitlines(True) # gotta do this to keep the newlines...

def load_local_data(filepath):
  # read the file into a list of strings
  with open(filepath, 'r') as f:
    rawdata = f.readlines()
  return rawdata

def load_data(args):
  if args.stationid:
    print "loading data from the web by station id..."
    rawdata = dl_from_waterservices(args.stationid)
  if args.file:
    print "loading data from a local file..."
    rawdata = load_local_data(args.file)

  print "  extract the station description line..."
  for index, line in enumerate(rawdata):
    if 'Data for the following' in line:
      station_desc = rawdata[index+1]
      station_desc = station_desc.split('#')[1].lstrip()

  print "  commenting header and data description line..."
  ucl = 0
  for index, line in enumerate(rawdata):
    if line[0] != '#':
      #print "found uncommented line.", ucl, index, line
      ucl = ucl+1
      rawdata[index] = '#'+line
      if ucl == 2:
        break

  print "  writing to temporary file..."
  
  tmpfname = 'hooch-tmp-rdbfile.txt'
  with open(tmpfname, 'w') as temp:
    temp.writelines(rawdata)
    temp.flush()
    print "  reading the temporary file with numpy.genfromtxt()..."
    data = np.genfromtxt( temp.name, 
                          comments='#', 
                          delimiter='\t', 
                          autostrip=True, 
                          usecols=(2,3,4),
                          #skiprows=2,
                          #names=True,
                          #names=('recdate', 'flow'),
                          missing_values=('Ice', 'Dis'),
                          #filling_values=None,
                          #dtype=('datetime64[D]', float),
                          dtype=[('recdate','datetime64[D]'), ('flow', float), ('quality', '|S10')],
                          filling_values=(np.datetime64('nat'), np.nan, 'ERROR') )
  try:
    print "  removing the temporary file..."
    os.remove(tmpfname)
  except OSError, e:
    print "Error: problem removing %s. %s" %(tmpfname, e)
    rasise
    
#   with tempfile.NamedTemporaryFile() as temp:
#     temp.writelines(rawdata)
#     temp.flush()
#     print "  reading the temporary file with numpy.genfromtxt()..."
#     data = np.genfromtxt( temp.name, 
#                           comments='#', 
#                           delimiter='\t', 
#                           autostrip=True, 
#                           usecols=(2,3,4),
#                           #skiprows=2,
#                           #names=True,
#                           #names=('recdate', 'flow'),
#                           missing_values=('Ice', 'Dis'),
#                           #filling_values=None,
#                           #dtype=('datetime64[D]', float),
#                           dtype=[('recdate','datetime64[D]'), ('flow', float), ('quality', '|S10')],
#                           filling_values=(np.datetime64('nat'), np.nan, 'ERROR') ) 
  print "  index the pandas DataFrame by date and time..."
  pdateidx = [dt64.tolist() for dt64 in data['recdate'] ]
  pseries = pd.Series(data['flow'], index=pdateidx)
  completeidx = pd.DatetimeIndex(start=pdateidx[0], end=pdateidx[-1], freq='D')

  # old simple 1D method
  #pdata = pd.DataFrame(data['flow'], index=pdateidx, columns=['discharge'])#, index=data['recdate'], columns=['date', 'flow'])
  
  # 2D
  print "  zip discharge and quality columns together..."
  pdata = pd.DataFrame(zip(data['flow'], data['quality']), 
                       index=pdateidx, columns=['discharge', 'quality'])

  print "  %s rows of data..." % pdata.shape[0]
  print "  %s non-null discharge readings" % pdata.discharge.count()
  print "  %s non-null quality readings" % pdata.quality.count()

  print "  dropping all 'P' and 'P:e' data..."
  pdata = pdata[pdata.quality != 'P']
  print "    ignoring discharge with 'P' in quality column. %s remaining" % (pdata.discharge.count())
  pdata = pdata[pdata.quality != 'P:e']
  print "    ignoring discharge with 'P:e' in quality column. %s remaining"  % (pdata.discharge.count())

  # if I don't do this then something breaks when building stats plots with multiple traces.
  print "  drop quality column..."
  pdata = pd.DataFrame(pdata.discharge, index=pdateidx, columns=['discharge'])

  # make this an actual pandas index
  print "  convert index to actual pandas.DatetimeIndex"
  pdata.index = pd.DatetimeIndex(pdata.index)#, freq='D')

  print "  expand data frame to include (empty) places for every point in period of record" 
  pdata = pdata.reindex(completeidx)


  print "  Period of Record: %s to %s" % ( pdata.index[0], pdata.index[-1])
  print "  %s days in period of record" % ( len(pdata.index) )
  print "  %s days with data in period of record" % ( pdata.discharge.count() )
  print "  %.2f%% of period of record has data" % (  (100 * (pdata.discharge.count() / float(len(pdata.index)))) ) 


  #print pdata
  #
  # Should result in something like this:
  # <class 'pandas.core.frame.DataFrame'>
  # DatetimeIndex: 1950 entries, 2008-06-01 00:00:00 to 2013-10-02 00:00:00
  # Freq: D
  # Data columns (total 1 columns):
  # discharge    1653  non-null values
  # dtypes: float64(1)
  
  print "  returning a pandas DataFrame and some metadata (%s discharge values)..." % pdata.discharge.count()
  metadata = {
    'station-desc': station_desc,
    'file': args.file
  }
  return pdata, metadata


# dealing with tick marks...
def handle_timeax_locs_and_labels(args):
  monthstart_locs, monthstart_labs = timeax_locs_and_labs(args)
  monthmid_locs, monthmid_labs = timeax_mm_locs_and_labs(args)
  
  print "    standard tick locations..."
  print "          Month start:", monthstart_locs
  print "                      ", monthstart_labs
  print "    15th day of month:", monthmid_locs
  print "                      ", monthmid_labs
  
  if args.wateryear:
    print "    adding marker for jan 1..."
    plt.axvline(91, linestyle='-', c='red') 
    
    print "    rolling the x axis labels..."
    ms_offsets      = [31, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30]
    ms_offsets      = np.roll(ms_offsets, 3)
    monthstart_locs = [np.sum(ms_offsets[0:i]) for i, x in enumerate(ms_offsets)]
    monthstart_labs = np.roll(monthstart_labs, 3)

    #mm_offsets      = [31, 30, 30, 31, 30, 31, 30, 31, 31, 30, 31, 30]
    monthmid_locs = [15+np.sum(ms_offsets[0:i]) for i, x in enumerate(ms_offsets)]
    if monthmid_labs:
      monthmid_labs = np.roll(monthmid_labs, 3)

  print "    tick locations after rolling for water year..."
  print "          Month start:", monthstart_locs
  print "                      ", monthstart_labs
  print "    15th day of month:", monthmid_locs
  print "                      ", monthmid_labs

  plt.xticks(monthstart_locs, monthstart_labs)

  from matplotlib.ticker import FixedLocator
  plt.axes().xaxis.set_minor_locator(FixedLocator(monthmid_locs))

def timeax_locs_and_labs(args):
  '''Return arrays of locations (in cords along x axis and corresponding labels
  for if using textual month labels (instead of julian days).'''
  # first day of each month in julian days (leap year) 'ms' -> 'Month Start'
  ms_locs = [1, 32, 61, 92, 122, 153, 183, 214, 245, 275, 306, 336]
  #ms_locs = [1,15, 32,45, 61,75, 92,106, 122,136, 153,167, 183,197, 214,228, 245,259, 275,289, 306,320 336,350]
  ms_locs = [x-1 for x in ms_locs]  # make it zero based

  if args.calmonth:
    ms_labs = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    #ms_labs = ['Jan','15','Feb','15','Mar','15','Apr','15','May','15','Jun','15','Jul','15','Aug','15','Sep','15','Oct','15','Nov','15','Dec','15'] 
  else:
    ms_labs = [x+1 for x in ms_locs] # index is zero based, but labels should be 1 based
  return (ms_locs, ms_labs)

def timeax_mm_locs_and_labs(args):
  #               *       *           *       *
  #   J   F   M   A   M   J   J   A   S   O   N   D
  #  [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

  ms_locs = [1, 32, 61, 92, 122, 153, 183, 214, 245, 275, 306, 336]
  ms_locs = [x-1 for x in ms_locs]

  # 15th of each month, regardless of length...
  mm_locs = [x+15 for x in ms_locs]
  #mm_locs = [15, 45, 75, 106, 136, 167, 197, 228, 259, 289, 320, 350]
  mm_labs = []
  return (mm_locs, mm_labs)


def pdts2ord(ts):
  '''Convert Pandas TimeStamp to ordinal (day of year)'''
  return pd.to_datetime(ts).date().timetuple().tm_yday

def tbc_df_summarytxt(df):
  return ''' Hooch Data Summary
  ------------------
  Date Range: %s
  Total # of Values: %s
  # of Undefined values: %s
  ''' % (1,12,4)   

def validate_julian_range(r):
  start = int(r[0])
  end = int(r[1])
  if start < 0 or end > 366 or start > end:
    return False
  else:
    return True
  
def print_pex_table(xlist, table):
  print '     %',
  for i in range(0, 10):
    print "%7i" % i,
  print

  for i in xlist:
    if i == 100:
      break
    if i%10 == 0:
      print '%6i' % i,
    print '%7.2f' % table[i],
    if (i % 10) == 9:
      print

def ord2md_leap(ord, fmt='%b %d'):
  ''' works for leap years...'''
  ord = ord - 1  # make ordinal days zero based
  base = dt.date(year=2012, month=1, day=1)
  new_date = base + dt.timedelta(days=ord)

  #print new_date.timetuple().tm_yday, " =?= ", ord + 1
  # make sure the conversion was correct...
  assert new_date.timetuple().tm_yday == ord + 1
  return new_date.strftime(fmt)

def md2ord_leap(m, d):
  base = dt.date(year=2012, month=m, day=d)
  return base.timetuple().tm_yday


@contextlib.contextmanager
def stdout_redirect(where):
  sys.stdout = where
  try:
    yield where
  finally:
    sys.stdout = sys.__stdout__

# def onclick(event):
#   print 'button=%d, x=%d, y=%d, xdata=%f, ydata=%f' % (
#         event.button, event.x, event.y, event.xdata, event.ydata)


#c = cmap.binary_r(i/10.0, 1) # force to grayscale 


# not used?
# def print_full_line_comments():
#   '''Prints each 'full line' i.e. starting with '#' comment in a file.'''
#   with open(f, 'r') as ff:
#     for line in ff.readlines():
#       if line[0] == '#':
#         print line,
# 


# needs to be part of a class...
# def onpick(self,event):
#     x = event.mouseevent.xdata
#     y = event.mouseevent.ydata
#     L =  self.ax.axvline(x=x)
#     self.fig.canvas.draw() 

# DEBUGGING

# paste (and uncomment) this in where ever you want to stop and look around.
#from IPython import embed
#embed()


if __name__ == "__main__":
  main()






'''Old notes from duration_curve'''

'''
In [52]: od_idx = numpy.array([pdts2ord(ts) for ts in df.index])

In [53]: ma = (od_idx>325) and (od_idx<355)

In [54]: df[ma].plot(style='o', marker='o')
Out[54]: <matplotlib.axes.AxesSubplot at 0x11407b650>
'''



'''
In [7]: user_ma = ((od_idx > 238) and (od_idx < 315))

In [8]: user_ma
Out[8]: array([False, False,  True, ...,  True, False, False], dtype=bool)

In [9]: df[user_ma].plot(marker='o')
Out[9]: <matplotlib.axes.AxesSubplot at 0x10863bc50>

In [10]: plt.show()
'''  



